import numpy as np

np.random.seed(0)

# three samples/instances - batch of three samples
X = [[1, 2, 3, 2.5],
     [2.0, 5.0, -1.0, 2.0],
     [1.5, 2.7, 3.3, -0.8]]

class Layer_Dense:
    def __init__(self, n_inputs, n_neurons):
        # randn takes the shape and so does np.zeros
        # n_inputs will be number of inputs i.e. the size of vector in the matrix X
        # n_neurons are no. of neurons in a layer
        self.weights = 0.10 * np.random.randn(n_inputs, n_neurons)
        self.biases = np.zeros((1, n_neurons))

    def forward(self, inputs):
        self.outputs = np.dot(inputs, self.weights) + self.biases


class Activation_ReLU:
    def forward(self, inputs):
        # ReLU produces 0 if input is less than or equal to 0
        # and 1 if input is greater than 0
        self.outputs = np.maximum(0, inputs)


# layer one having 5 neurons
layer1 = Layer_Dense(4, 5)
# layer two having 2 neurons
# layer2 = Layer_Dense(5, 2)

layer1.forward(X)
print(layer1.outputs)

activation1 = Activation_ReLU()
activation1.forward(layer1.outputs)
# 0/neg inputs will produce 0 as output
print(activation1.outputs)

# layer2.forward(layer1.outputs)
# print(layer2.outputs)
