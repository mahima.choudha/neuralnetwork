import numpy as np

'''
This is a neural network having one layer,
3 neurons and 3 samples
layer == number of weights matrix
neuron == number of vectors in weights matrix
sample == number of instances in the inputs matrix
'''

# three samples/instances - batch of three samples
inputs = [[2, 4, 3, 1],
          [2.2, 1.4, 2.3, 1.5],
          [3.0, 2.7, 3.2, 0.1]]

# number of lists in weights are equivalent to number of neurons in the network
weights = [[0.23, 0.14, 0.45, 0.54],
           [0.21, -0.7, 0.15, -0.42],
           [-0.31, 0.21, -0.28, 0.32]]

# weights and bias will not change with input size as wgt and bias are corresponds to neurons
biases = [2, 3, 4]

# np.dot --> 0.23*2 + 0.14*4 + 0.45*3 + 0.54*1
# as both inputs and weights are matrix, np.dot will throw a "shape" error
# shape of input -> (3,4) | shape of weights -> (3,4)
# np.dot expects and NN requires => shape of input -> (3,4) | shape of weights -> (4,3)
# to achieve this we'll have to transpose the weights
output = np.dot(inputs, np.array(weights).T) + biases
print(output)
